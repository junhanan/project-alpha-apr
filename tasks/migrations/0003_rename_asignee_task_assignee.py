# Generated by Django 4.2 on 2023-04-25 19:33

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tasks", "0002_rename_assignee_task_asignee"),
    ]

    operations = [
        migrations.RenameField(
            model_name="task",
            old_name="asignee",
            new_name="assignee",
        ),
    ]
